/**
 * Namespace for browser action functionality.
 */
var browseraction = {};


/**
 * Initializes UI elements in the browser action popup.
 */
browseraction.initialize = function() {
  browseraction.installButtonClickHandlers_();
  browseraction.listenForRequests_();
};

/** @private */
browseraction.installButtonClickHandlers_ = function() {
  $('#show_options').on('click', function() {
    chrome.tabs.create({'url': 'options.html'});
  });
  
  $('#sync_now').on('click', function() {
    chrome.extension.sendMessage({method: 'events.product.fetch'},
        browseraction.showEventsFromFeed_);
  });
};

/**
 * Listens for incoming requests from other pages of this extension and calls
 * the appropriate (local) functions.
 * @private
 */
browseraction.listenForRequests_ = function() {
  chrome.extension.onMessage.addListener(function(request, sender, opt_callback) {
    switch(request.method) {
      case 'ui.refresh':
        chrome.extension.sendMessage({method: 'events.product.get'},
            browseraction.showEventsFromFeed_);
        break;
    }
  });
};

/**
 * Retrieves events from the calendar feed, sorted by start time, and displays
 * them in the browser action popup.
 * @param {Array} events The events to display.
 * @private
 */
browseraction.showEventsFromFeed_ = function(events) {
  $('#products-list').empty();
  
  // If there are no events today, then avoid showing an empty date section.
  if (!events || events.length == 0) {
    $('<div>').addClass('no-events-today')
        .append(chrome.i18n.getMessage('no_events_today'))
        .appendTo($('#products-list'));
    return;
  }

  for (var i = 0; i < events.length; i++) {
    var event = events[i];
    $('<div>').addClass('product-header')
        .text(event.name)
        .appendTo($('#products-list'));

    best_price = null;
    best_vendor = null;
    result_keys = Object.keys(event.latest_results);
    if (event.latest_results && result_keys.length > 0) {
      for (var k = 1; k <= result_keys.length; k++) {
        var key = result_keys[k-1];
        var item = event.latest_results[key];
        if (!best_price || best_price > item.result) {
          best_vendor = key;
          best_price = item.result;
        }
      }
    }

    $('<div>').addClass('product-last-price')
      .text((best_price) ? best_price + ' @ ' + best_vendor : 'No price found.')
      .appendTo($('#products-list'));

    browseraction.createEventDiv_(event).appendTo($('#products-list'));
  }
};


/**
 * Creates a <div> that renders a detected event or a fetched event.
 * @param {CalendarEvent} event The calendar event.
 * @return {!jQuery} The rendered 'Add to Calendar' button.
 * @private
 */
browseraction.createEventDiv_ = function(event) {
  var eventDiv = /** @type {jQuery} */ ($('<div>')
      .addClass('product'));

  var eventDetails = $('<div>')
      .addClass('product-details')
      .appendTo(eventDiv);

  var eventTitle = $('<div>').addClass('event-title');
  eventTitle.appendTo(eventDetails);

  return eventDiv;
};

/**
 * When the popup is loaded, fetch the events in this tab from the
 * background page, set up the appropriate layout, etc.
 */
window.addEventListener('load', function() {
  browseraction.initialize();
}, false);
