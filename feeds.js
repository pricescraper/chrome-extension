// Copyright 2012 and onwards Google Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @fileoverview Retrieves and parses a calendar feed from the server.
 * @author manas@google.com (Manas Tungare)
 */

/**
 * The Calendar Feed Parser namespace.
 * @namespace
 */
var feeds = {};


/**
 * All products from visible calendars obtained during the last fetch.
 * @type {Array.<Object>}
 */
feeds.products = [];


/**
 * The time at which fresh data was last fetched from the server.
 * @type {Date}
 */
feeds.lastFetchedAt = null;


/**
 * Sends a request to fetch the list of calendars for the currently-logged in
 * user. When calendars are received, it automatically initiates a request
 * for events from those calendars.
 */
feeds.fetchProducts = function() {
  background.log('feeds.fetchProducts()');

  var api_url = options.get(options.Options.API_URL);
  if (!api_url) {
    background.log('API url not defined in settings.');
    return;
  }
  background.log('options[api-url]:', api_url);

  var feedUrl = api_url.concat('/products/');
  background.log(feedUrl);
  $.ajax(feedUrl, {
    headers: {
      'Accept': 'application/json'
    },
    success: function(data) {
      background.log('Received events, now parsing.', data);
      var allProducts = [];
      for (var i = 0; i < data.length; i++) {
        var productEntry = data[i];
        allProducts.push(productEntry);
      }

      feeds.products = allProducts;
      background.log('Products reveived :', feeds.products);
      feeds.refreshUI();
    },
    error: function(response) {
      background.log('Fetch Error (Events)', response.statusText);
    }
  });
};


/**
 * Updates the 'minutes/hours/days until' visible badge from the events
 * obtained during the last fetch. Does not fetch new data.
 */
feeds.refreshUI = function() {
  // Notify the browser action in case it's open.
  chrome.extension.sendMessage({method: 'ui.refresh'});
};
