// Copyright 2010 and onwards Google Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @fileoverview Script that runs in the context of the background page.
 * @author manas@google.com (Manas Tungare)
 */

/**
 * The namespace for background page related functionality.
 * @namespace
 */
var background = {};

/**
 * An in-memory log that stores the last N records.
 * @type {Array.<string>}
 * @private
 */
background.logs_ = [];


/**
 * A function that logs all its arguments to memory and to the console if the
 * user has enabled logging.
 * @param {string} message The message to log.
 * @param {*=} opt_dump An optional set of parameters to show in the console.
 */
background.log = function(message, opt_dump) {
  //if (options.get(options.Options.DEBUG_ENABLE_LOGS)) {
    var timestampedMessage = '[' + moment().toISOString() + '] ' + message;
    if (opt_dump) {
      background.logs_.push(timestampedMessage + ' ' + JSON.stringify(opt_dump, null /* replacer */, '  '));
      window.console.log(timestampedMessage, opt_dump);
    } else {  // Otherwise the log shows a spurious string "undefined" for every opt_dump.
      background.logs_.push(timestampedMessage);
      window.console.log(timestampedMessage);
    }
  //}
};
/**
 * Initializes the background page by registering listeners.
 */
background.initialize = function() {
  background.listenForRequests_();
  scheduler.start();
};

/**
 * Listens for incoming RPC calls from the browser action and content scripts
 * and takes the appropriate actions.
 * @private
 */
background.listenForRequests_ = function() {
  chrome.extension.onMessage.addListener(function(request, sender, opt_callback) {
    switch(request.method) {
      case 'events.product.get':
        if (opt_callback) {
          opt_callback(feeds.products);
        }
        break;

      case 'events.product.fetch':
        feeds.fetchProducts();
        break;

      case 'options.changed':
        feeds.refreshUI();
        break;
    }

    // Indicates to Chrome that a pending async request will eventually issue
    // the callback passed to this function.
    return true;
  });
};


background.initialize();
